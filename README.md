### About This Course
This course shows the importance of data chunking when working with spatio-temporal gridded data sets. Data sets for the Earth system sciences are getting larger and larger, and they can no longer be completely loaded into working memory. The programming language used for the course is Julia. In brief, we will learn how to load files with different chunks and explore their properties. We will compute global per-pixel statistics and time steps and compare data processing performance for different chunks. Finally, we will demonstrate the relevance of chunking when processing data for different data axes.
### Level
Intermediate
### Requirements
programming experience, experience in working with gridded data
### Subject Area
Climate science and modelling, remote sensing, ecological modelling, biogeochemistry
### What You Will Learn
- Lecture on understanding data chunking
- Introduction to data chunking
- Data exploration
- Statistical computation
- Final remarks and scripts
### Resources
[Understanding data chunking](https://doi.org/10.5281/zenodo.7871119) by Lina M. Estupinan-Suarez, Felix Cremer, Fabian Gans
### Administration
Farzaneh Sadeghi

---

This content is based on [Tutorial: Understanding Data Chunking](https://github.com/linamaes/chunking_tutorial?tab=readme-ov-file#tutorial-understanding-data-chunking) by Lina M. Estupinan-Suarez, Felix Cremer, Fabian Gans, which is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/). DOI: [https://doi.org/10.5281/zenodo.7871119](https://doi.org/10.5281/zenodo.7871119). Modifications were made to the original content. This modified content is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
